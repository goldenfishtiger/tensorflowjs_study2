FROM docker.io/goldenfishtiger/sinatra_skelton

LABEL maintainer="Cnes Taro <goldenfishtiger@gmail.com>"

ARG http_proxy
ARG https_proxy

USER root
WORKDIR /

# https://medium.com/nttlabs/ubuntu-21-10-and-fedora-35-do-not-work-on-docker-20-10-9-1cd439d9921
#SHELL ["/clone3-workaround", "/bin/bash", "-c"]

RUN set -x \
	&& dnf install -y \
        nodejs \
	&& rm -rf /var/cache/dnf/* \
	&& dnf clean all

# git clone tensorflowjs_study2 しておくこと
ARG TARGET=tensorflowjs_study2
RUN set -x \
    && ln -s /home/user/sinatra_skelton /home/user/${TARGET}
USER user
WORKDIR /home/user
RUN set -x \
	&& npm install \
		@tensorflow/tfjs-node \
		jpeg-js \
		coffeescript

USER root
COPY ${TARGET} /home/user/${TARGET}
RUN set -x \
	&& chown -R user:user /home/user/${TARGET}/ \
	&& ln -sv /home/user/node_modules/coffeescript/bin/coffee /usr/local/bin

EXPOSE 8080

# Dockerfile 中の設定スクリプトを抽出するスクリプトを出力、実行
COPY Dockerfile .
RUN echo $'\
cat Dockerfile | sed -n \'/^##__BEGIN0/,/^##__END0/p\' | sed \'s/^#//\' > startup.sh\n\
' > extract.sh && bash extract.sh

# docker-compose up の最後に実行される設定スクリプト
##__BEGIN0__startup.sh__
#
#	CONFIG=sinatra.config
#	if [ ! -e pv/$CONFIG ]; then
#		echo "# $CONFIG.sample set up."
#		cp -av $CONFIG.sample pv
#		echo "Rename 'pv/$CONFIG.sample' to 'pv/$CONFIG' and modify it."
#		echo '**** HALT ****'
#		sleep infinity
#	fi
#
#	if [ -e pv/dot.bashrc ]; then
#		cp -av pv/dot.bashrc /home/user/.bashrc
#		cp -av pv/dot.virc /home/user/.virc
#		cp -av pv/dot.gitconfig /home/user/.gitconfig
#	fi
#
#	s=S
#	while true; do
#		pgrep -f puma > /dev/null
#		if [ $? -ne 0 ]; then
#			echo "`date`: ${s}tart puma."
#			bundle exec rackup -P /tmp/rack.pid --host 0.0.0.0 --port 8080 &
#		fi
#		s=Res
#		sleep 5
#	done
#
##__END0__startup.sh__

USER user
WORKDIR /home/user/${TARGET}

ENTRYPOINT ["bash", "-c"]
CMD ["bash startup.sh"]

