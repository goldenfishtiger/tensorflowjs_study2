#!/usr/bin/env ruby

# cat classification.result | grep 'sports car' | ./conv2html.rb > sports_car.html

img2caps = {}; $stdin.each {|l|
	l =~ /^$/ and next
	l =~ /^#/ and next

	ls = l.chomp.split(/, /)
	uri = ls[1].gsub(/^pv/, 'images')

	img2caps[uri] ||= []
	img2caps[uri] << ls[0]
}

puts("<TABLE border='1'>\n")
n = 0; col = 5; max = 9999; img2caps.each {|uri, caps|
	((max -= 1) < 0) and break
#	caps.join(', ') =~ /sports car/ or next
	puts("<TR>") if((n += 1) % col == 1)
	puts("<TD><IMG src='%s'><BR>%s" % [uri, caps.join(', ')])
}
puts("</TABLE>\n")

__END__

