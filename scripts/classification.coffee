#!/usr/bin/node

# coffee -b -c classification.coffee; mv -f classification.js classification.mjs; node classification.mjs

import * as tf from '@tensorflow/tfjs-node'
import * as fs from 'fs'
import * as jpeg from 'jpeg-js'

find = (path, res) ->
	for entry in fs.readdirSync(path)
		stats = fs.statSync(path + '/' + entry)
		if(stats.isDirectory())
			find(path + '/' + entry, res)
		else
			if(entry.match(/\.JPE?G$/i))
				res.push(path + '/' + entry)

mysteries = []; find('pv/photos', mysteries)

import { INCEPTION_CLASSES } from './labels.mjs'

n = 0; skip = 0; max = 9999
tf.ready().then(() ->
	modelPath =
		'https://tfhub.dev/google/tfjs-model/imagenet/inception_v3/classification/3/default/1'
	tf.tidy(() ->
		tf.loadGraphModel(modelPath, { fromTFHub: true }).then((model) ->
			fd = fs.openSync('pv/classification.result', 'a')
			for mystery in mysteries
				console.log(Date().toString(), n += 1, mystery)
				continue unless((skip -= 1) < 0)
				break if((max -= 1) < 0)
				fs.writeSync(fd, '# ' + n + '\n')

				mysteryImage = jpeg.decode(fs.readFileSync(mystery))
				myTensor = tf.browser.fromPixels(mysteryImage)
				# Inception v3 expects an image resized to 299x299
				readyfied = tf.image
					.resizeBilinear(myTensor, [299, 299], true)
					.div(255)
					.reshape([1, 299, 299, 3])

				result = model.predict(readyfied)
				result.print() # useless

				{ values, indices } = tf.topk(result, 3)
				indices.print()

				# Let's hear those winners
				winners = indices.dataSync()
				console.log("""
					🥇 First place #{INCEPTION_CLASSES[winners[0]]},
					🥈 Second place #{INCEPTION_CLASSES[winners[1]]},
					🥉 Third place #{INCEPTION_CLASSES[winners[2]]}
				""")

				fs.writeSync(fd, INCEPTION_CLASSES[winners[0]] + ', ' + mystery + ', 1\n')
				fs.writeSync(fd, INCEPTION_CLASSES[winners[1]] + ', ' + mystery + ', 2\n')
				fs.writeSync(fd, INCEPTION_CLASSES[winners[2]] + ', ' + mystery + ', 3\n')

				myTensor.dispose()
#				console.log(tf.memory().numTensors, tf.memory().numBytes)
		)
	)
)

